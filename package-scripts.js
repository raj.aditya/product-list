/**
 * Windows: Please do not use trailing comma as windows will fail with token error
 */

const { series, rimraf } = require('nps-utils');
const fs = require('fs-extra');

function eslint(path) {
    return `eslint ${path} --ext .js,.jsx,.ts,.tsx`;
}
const eslintFix = path => {
    return `eslint ${path} --fix`;
};

const prettier = path => {
    return `prettier --check ${path}`;
};

const prettierFix = path => {
    return `prettier ${path} --write`;
};

module.exports = {
    scripts: {
        default: 'nps start',
        /**
         * Starts the builded app from the dist directory.
         */
        production: {
            script: 'cross-env NODE_ENV=production node dist/app.js',
            description: 'Starts the builded app',
        },
        /**
         * Serves the current app and watches for changes to restart it
         */
        serve: {
            inspector: {
                script: series(
                    'nps banner.serve',
                    'nodemon --watch src --watch --inspect'
                ),
                description: 'Serves the current app and watches for changes to restart it, you may attach inspector to it.'
            },
            script: series(
                'nps banner.serve',
                'nodemon --watch src --watch '
            ),
            description: 'Serves the current app and watches for changes to restart it'
        },
       
        config: {
            script: series(
                runFast('./commands/tsconfig.ts'),
            ),
            hiddenFromHelp: true
        },
        lint: {
            script: eslint(`./src/**/*.ts`),
            hiddenFromHelp: true
        },
        lintFix: {
            script: eslintFix(`./src/`),
            hiddenFromHelp: true,
        },
        generateapidoc: {
            script: series(
                'apidoc -i src -o src/public/apidoc',
            ),
            description: 'Setup`s the development environment(npm & database)'
        },
        /**
         * Transpile your app into javascript
         */
        transpile: {
            script: `tsc --project ./tsconfig.build.json`,
            hiddenFromHelp: true
        },
        /**
         * Builds the app into the dist directory
         */
        build: {
            script: series(
                'nps clean.dist',
                'nps banner.serve',
                'nps config',
                'nps lint',
                'nps transpile',
                'nps generateapidoc',
                'nps copy',
                'nps copy.tmp',
                'nps clean.tmp',
            ),
            description: 'Builds the app into the dist directory'
        },
        copy: {
            default: {
                script: series(
                    `nps copy.public`,
                    `nps copy.apidoc`
                ),
                hiddenFromHelp: true
            },
            tmp: {
                script: copyDir(
                    './.tmp/src/',
                    './dist/'
                ),
                hiddenFromHelp: true
            },
            apidoc: {
                script: copyDir(
                    './src/public/',
                    './dist/public/'
                ),
                hiddenFromHelp: true
            },
            public: {
                script: copy(
                    './src/public/*',
                    './dist/'
                ),
                hiddenFromHelp: true
            }
        },
        clean: {
            dist: {
                script: rimraf('./dist'),
                hiddenFromHelp: true
            },
            tmp: {
                script: rimraf('./.tmp'),
                hiddenFromHelp: true
            }
        },
        banner: {
            build: banner('build'),
            serve: banner('serve'),
            testUnit: banner('test.unit'),
            testIntegration: banner('test.integration'),
            testE2E: banner('test.e2e'),
        }
        
    }
};

function banner(name) {
    return {
        hiddenFromHelp: true,
        silent: true,
        description: `Shows ${name} banners to the console`,
        script: runFast(`./commands/banner.ts ${name}`),
    };
}

function copy(source, target) {
    return `copyfiles --up 1 ${source} ${target}`;
}

function copyFile(source, target) {
    return `ncp ${source} ${target}`;
}

function copyDir(source, target) {
    return `ncp ${source} ${target}`;
}
function runFast(path) {
    return `ts-node --transpileOnly ${path}`;
}

