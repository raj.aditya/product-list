var rootDir = process.env.NODE_ENV === 'production' ? 'dist' : 'src';
var env = require(process.cwd()+ "/" + rootDir +'/env').env;
var fExt = process.env.NODE_ENV === 'production' ? '.js' : '.ts';
var seedFileExt = process.env.NODE_ENV === 'production' ? '.seed' : '.seed';
module.exports = {
    type: env.db.type,
    host: env.db.host,
    port: env.db.port,
    username: env.db.username,
    password: env.db.password,
    database: env.db.database,
    synchronize: env.db.synchronize,
    logging: env.db.logging,
    seeds: [rootDir + '/api/seeders/**/*'+seedFileExt+fExt],
    entities: [rootDir + '/api/models/**/*'+fExt],
    migrations: [rootDir + '/api/migration/**/*'+fExt],
    subscribers: [rootDir + '/subscriber/**/*'+fExt],
    cli: {
        entitiesDir: rootDir + '/api/models/',
        migrationsDir: rootDir + '/api/migration',
        subscribersDir: rootDir + '/api/subscriber'
    }
};
