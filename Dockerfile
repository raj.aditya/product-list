FROM node:12.18-slim AS build  

WORKDIR /usr/src/productlist

COPY . .
RUN rm /usr/src/productlist/package-lock.json
RUN npm install
RUN npm run build


FROM node:12.18-slim AS release  
WORKDIR /usr/src/productlist

COPY ./package.json ./
RUN npm install --production

COPY ./ormconfig.js ./ormconfig.js
COPY ./.env ./.env 
COPY ./tsconfig.json ./tsconfig.json
COPY --from=build /usr/src/productlist/dist ./dist/
COPY ./package-scripts.js ./package-scripts.js 
COPY ./entrypoint.sh ./entrypoint.sh

RUN chmod +x ./entrypoint.sh
