rm -rf node_modules
rm -rf logs
sudo chown -R ${USER} ${HOME}/.npm
npm install
npm run build
npm run migration
npm run seed
./nginx_config.sh
./supervisorCreator.sh
./ubuntu_supervisor.sh
