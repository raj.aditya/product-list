import * as dotenv from 'dotenv';
import * as path from 'path';
import * as PostgressConnectionStringParser from 'pg-connection-string';
import * as pkg from '../package.json';
import {
  getOsEnv,
  getOsEnvOptional,
  getOsPath,
  getOsPaths,
  normalizePort,
  toBool
} from './lib/env';

/**
 * Load .env file or for tests the .env.test file.
 */
dotenv.config({
  path: path.join(
    process.cwd(),
    `./.env${process.env.NODE_ENV === 'test' ? '' : ''}`
  ),
});
/**
 * Environment variables
 */

const databaseUrl: string = getOsEnv('APP_DB_URL');
const connectionOptions: any = PostgressConnectionStringParser.parse(
  databaseUrl
);
export const env = {
  node: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  isDevelopment: process.env.NODE_ENV === 'development',
  app: {
    name: getOsEnv('APP_NAME'),
    version: (pkg as any).version,
    description: (pkg as any).description,
    host: getOsEnv('APP_HOST'),
    schema: getOsEnv('APP_SCHEMA'),
    routePrefix: getOsEnv('APP_ROUTE_PREFIX'),
    port: normalizePort(process.env.PORT || getOsEnv('PORT')),
    banner: toBool(getOsEnv('APP_BANNER')),
    dirs: {
      migrations: getOsPaths('APP_MIGRATIONS'),
      migrationsDir: getOsPath('APP_MIGRATIONS_DIR'),
      entities: getOsPaths('APP_ENTITIES'),
      entitiesDir: getOsPath('APP_ENTITIES_DIR'),
      controllers: getOsPaths('APP_CONTROLLERS'),
      middlewares: getOsPaths('APP_MIDDLEWARES'),
      interceptors: getOsPaths('APP_INTERCEPTORS'),
      subscribers: getOsPaths('APP_SUBSCRIBERS'),
      resolvers: getOsPaths('APP_RESOLVERS'),
    },
  },
  log: {
    level: getOsEnv('LOG_LEVEL'),
    output: getOsEnv('LOG_OUTPUT'),
  },

  db: {
    type: getOsEnv('APP_CONNECTION'),
    host: connectionOptions.host,
    port: connectionOptions.port,
    username: connectionOptions.user,
    password: connectionOptions.password,
    database: connectionOptions.database,
    synchronize: toBool(getOsEnvOptional('APP_SYNCHRONIZE')),
    logging: toBool(getOsEnv('APP_LOGGING')),
    logger: getOsEnv('APP_LOGGER')
  },
  apidoc: {
    enabled: toBool(getOsEnv('APIDOC_ENABLED')),
    route: getOsEnv('APIDOC_ROUTE'),
  },
};

