import { createConnection, getConnectionOptions, useContainer } from 'typeorm';
import { Container } from 'typedi';
import { env } from '../env';

export const typeormLoader = async () => {
  useContainer(Container);
  const loadedConnectionOptions = await getConnectionOptions();
  const connectionOptions = Object.assign(loadedConnectionOptions, {
    type: env.db.type as any, // See createConnection options for valid types
    host: env.db.host,
    port: env.db.port,
    username: env.db.username,
    password: env.db.password,
    database: env.db.database,
    synchronize: env.db.synchronize,
    logging: env.db.logging,
    logger: env.db.logger,
    entities: env.app.dirs.entities,
    migrations: env.app.dirs.migrations,
  });

  return createConnection(connectionOptions);
};
