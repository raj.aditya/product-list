import express, { Application } from 'express';

import path from 'path';
import favicon from 'serve-favicon';
import * as bodyParser from 'body-parser';
import { useExpressServer } from 'routing-controllers';

import { env } from '../env';

const {
  app: {
    name,
    version,
    description,
    routePrefix,
    dirs: { controllers, middlewares },
  },
} = env;
export const expressLoader = () => {
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(express.static(path.join(__dirname, '../../assets')));
  app.set('view engine', 'html');
  const expressApp: Application = useExpressServer(app, {
    cors: true,
    classTransformer: true,
    routePrefix,
    defaultErrorHandler: false,
    /**
     * We can add options about how routing-controllers should configure itself.
     * Here we specify what controllers should be registered in our express server.
     */
    controllers,
    middlewares,
  });
  // Run application to listen on given port
  expressApp.get(routePrefix, (req: express.Request, res: express.Response) => {
    return res.json({
      name,
      version,
      description,
    });
  });

  // Serve static filles like images from the public folder
  // A favicon is a visual cue that client software, like browsers, use to identify a site
  expressApp
    .use(
      express.static(path.join(__dirname, '..', 'public'), {
        maxAge: 31557600000,
      }),
    )
    .use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));

  return expressApp;
};

export default expressLoader;
