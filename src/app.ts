import 'reflect-metadata';
import { banner } from './lib/banner';
import { Logger } from './lib/logger';
import { env } from './env';
import { expressLoader } from './loaders/expressLoader';
import { winstonLoader } from './loaders/winstonLoader';
import { typeormLoader } from './loaders/typeormLoader';
import { iocLoader } from './loaders/iocLoader';

const log = new Logger(__filename);
winstonLoader();
iocLoader();
typeormLoader()
  .then((connection) => {
    const expressApp = expressLoader();
    expressApp.listen(env.app.port, () => {
      banner(log);
    });
  })
  .catch((error) => {
    log.error(`Application is crashed: ${error}`);
  });
