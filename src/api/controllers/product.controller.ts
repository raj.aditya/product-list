import { Product } from '../models/Product';
import { CommonUtility } from '../utility/common.utility';
import { ProductUtility } from '../utility/product.utility';
import {
  Get,
  JsonController,
  Param,
  QueryParams,
  Req,
  Res,
} from 'routing-controllers';


@JsonController('/product')
export class ProductController {
  constructor(
    private productUtility: ProductUtility,
    private commonUtility: CommonUtility
  ) {}

  // Product List API
  /**
   * @api {get} /api/product/list Product List API
   * @apiGroup Product
   * @apiParam (Request Body) {Number} limit Limit of the product List `Pagination`.
   * @apiParam (Request Body) {Number} offset Skip Number of the product List `Pagination`.
   * @apiParam (Request Body) {Number} count To get count of the product List `Total count`.
   * @apiParam (Request Body) {String} search Key to search product list by given `search` key.
   * @apiParam (Request Body) {String} sort Key to sort product list by given `sort` key Sort By `New` not supported.
   * @apiParam (Request Body) {String} filter Key to filter product list by given `filter` object. Use double quotes`"` around key and values.
   * @apiParamExample {QueryParams} Input
   * {
   *     /api/product/list/?limit=10&offset=10&count=0&sort=price_asc&search=road&filter={"brand": "Puma", "gender": ["Men", "Women"]}
   * }
   * @apiSuccessExample {json} Success
   * HTTP/1.1 200 OK
   * {
   *      "message": "Successfully get products list",
   *      "data": [{}]
   *      "status": true
   * }
   * @apiSampleRequest /api/product/list
   * @apiErrorExample {json} Product List error
   * HTTP/1.1 500 Internal Server Error
   */
  @Get('/list')
  public async productList(@QueryParams() productParams: any, @Res() response: any, @Req() request: any): Promise<any> {
      
    try {
      let relations = [];
      let fields = [];
      let productList: Product[]  = await this.productUtility.getProductList(productParams, fields, relations);
      return this.commonUtility.sendResponseBody(true, "Successfully get products list", 200, response, productList);
    } catch (error) {
      console.error("Error in getting product", error)
      return this.commonUtility.sendResponseBody(false, "Some issue in getting products", 400, response);
    }
  }

  // Get Product Detail API
  /**
   * @api {get} /api/product/detail/:id Product Detail API
   * @apiGroup Product
   * @apiParam (Request Body) {Number} id Id of the product to get detail.
   * @apiParamExample {Param} Input
   * {
   *     /api/product/detail/1
   * }
   * @apiSuccessExample {json} Success
   * HTTP/1.1 200 OK
   * {
   *      "message": "Successfully get product detail",
   *      "data": []
   *      "status": true
   * }
   * @apiSampleRequest /api/product/detail/:id
   * @apiErrorExample {json} Product Detail error
   * HTTP/1.1 500 Internal Server Error
   */
  @Get('/detail/:id')
  public async productDetail(@Param('id') pId, @Res() response: any, @Req() request: any): Promise<any> {
      
    try {
      let relations = ['images', 'inventoryInfos', 'systemAttributes'];
      let productdetail: Product  = await this.productUtility.getProductDetail(pId, relations);
      if(productdetail){
        return this.commonUtility.sendResponseBody(true, "Successfully get product detail", 200, response, productdetail);
      } else {
        return this.commonUtility.sendResponseBody(true, "Product Id wrong", 400, response, productdetail);
      }
      
    } catch (error) {
      return this.commonUtility.sendResponseBody(false, "Some issue in getting product detail", 500, response, undefined);
    }
  }

  // Get Sort API
  /**
   * @api {get} /api/product/sort Sort Body API
   * @apiGroup Product
   * @apiSuccessExample {json} Success
   * HTTP/1.1 200 OK
   * {
   *      "message": "Successfully get sort body",
   *      "data": []
   *      "status": true
   * }
   * @apiSampleRequest /api/product/sort
   * @apiErrorExample {json} Sort Body error
   * HTTP/1.1 500 Internal Server Error
   */
  @Get('/sort')
  public async sortBody(@Res() response: any, @Req() request: any): Promise<any> {
      
    try {
      let sortSystemConfig  = await this.productUtility.getSystemConfigData('sort');
      let sortBody = JSON.parse(sortSystemConfig.value);
      return this.commonUtility.sendResponseBody(true, "Successfully get sort body", 200, response, sortBody);
    } catch (error) {
      return this.commonUtility.sendResponseBody(false, "Some issue in getting sort body", 400, response, undefined)
    }
  }

  // Get Filter API
  /**
   * @api {get} /api/product/filter Filter Body API
   * @apiGroup Product
   * @apiSuccessExample {json} Success
   * HTTP/1.1 200 OK
   * {
   *      "message": "Successfully get filter body",
   *      "data": []
   *      "status": true
   * }
   * @apiSampleRequest /api/product/filter
   * @apiErrorExample {json} Filter Body error
   * HTTP/1.1 500 Internal Server Error
   */
  @Get('/filter')
  public async filterBody(@Res() response: any, @Req() request: any): Promise<any> {
      
    try {
      let filterSystemConfig  = await this.productUtility.getSystemConfigData('filter');
      let filterBody = JSON.parse(filterSystemConfig.value);
      return this.commonUtility.sendResponseBody(true, "Successfully get filter body", 200, response, filterBody);
    } catch (error) {
      return this.commonUtility.sendResponseBody(false, "Some issue in getting filter body", 400, response, undefined);
    }
  }

  

}
