import {
    JsonController,
    Get,
  } from 'routing-controllers';

  @JsonController('/general')
  export class UtilsController {
    constructor() {}

    /**
     * @api {get} /api/general/ping PING
     * @apiGroup General
     * @apiSuccessExample {string} PONG
     * HTTP/1.1 200 OK
     * {
     *      "PONG"
     * }
     * @apiSampleRequest /api/general/ping
     * @apiErrorExample {json}  error
     * HTTP/1.1 500 Internal Server Error
     */
    @Get('/ping')
    public hellCheck() {
        return 'PONG';
    }
}
  