import { Service } from 'typedi';
import { PRODUCT_URL } from '../config/constant';
import fetch from 'node-fetch';
import { Avatar } from '../models/Avatar';
import { Product } from '../models/Product';
import { InventoryInfo } from '../models/InventoryInfo';
import { SystemAttribute } from '../models/SystemAttributes';
import { SystemConfig } from '../models/SystemConfig';

@Service()
export class DbSetupUtility {
    constructor() {}

    /**
     * Get Product Json using URL
    */
    public async getProductReponse(): Promise<any> {
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        const options = {
            method: 'GET',
            headers: headers
        };
        const res =  await fetch(PRODUCT_URL, options);
        return res.json();
    }

    /**
     * Set up Db from Product dump using URL
    */
    public setDbFromProductDump(jsonResult) {
        
        let products: Product[] = [];
        let imageList: Avatar[] = [];
        let inventoryList: InventoryInfo[] = [];
        let sysInfoList: SystemAttribute[] = [];
        let systemConfigs: SystemConfig[] = [];
        this.parseDataAndCreateObjects(jsonResult, products, imageList, inventoryList, sysInfoList, systemConfigs);
        return {
            products: products,
            imageList: imageList,
            inventoryList: inventoryList,
            sysInfoList: sysInfoList,
            systemConfigs: systemConfigs
        }
    }

    private parseDataAndCreateObjects(jsonResult, products: Product[], imageList: Avatar[], 
                                      inventoryList: InventoryInfo[], sysInfoList: SystemAttribute[],
                                      systemConfigs: SystemConfig[]
    ) :void {
        let sortOptions = jsonResult["sortOptions"];
        let genderArr: string[] = [];
        let brandArr: string[] = [];
        let categoryArr: string[] = [];
        jsonResult["products"].forEach(productJson => {
            let product = this.parseProductAndCreateObj(productJson, products);
            this.parseImageAndCreateObject(productJson["images"], product, imageList);
            this.parseInventoryInfoAndCreateObject(productJson["inventoryInfo"], product, inventoryList);
            this.parseSystemAttributesAndCreateObject(productJson["systemAttributes"], product, sysInfoList);
            this.createFilterAndSortBody(product, genderArr, brandArr, categoryArr);
        });
        this.parseAndCreateSortData(sortOptions, systemConfigs);
        this.parseAndCreateFilterData(genderArr, brandArr, categoryArr, systemConfigs);
    }

    private parseAndCreateSortData(sortOptions: string[], systemConfigs: SystemConfig[]) :void{
        let sortValues = [];
        for (const sOption of sortOptions) {
            let key = '';
            switch (sOption) {
                case 'new':
                    key = 'New';
                    break;
                case 'price_desc':
                    key = 'Price-high to low';
                    break;
                case 'price_asc':
                    key = 'Price-low to high';
                    break;
                case 'popularity':
                    key = 'Popularity';
                    break;
                case 'discount':
                    key = 'Discount';
                    break;
                case 'delivery_time':
                    key = 'Delivery time'
                    break;
                default:
                    break;
            }
            sortValues.push({
                key: key,
                value: sOption
            });
        }
        let sortObj = this.createSystemconfigObj("sort", sortValues);
        systemConfigs.push(sortObj);
    }

    private parseAndCreateFilterData(genderArr: string[], brandArr :string[], 
                                     categoryArr :string[], systemConfigs: SystemConfig[]
    ) :void{
        const filterBody: object = {
            Gender: {key:'gender', value: genderArr},
            Brand: {key:'brand', value: brandArr},
            Category: {key:'category', value: categoryArr}
        };
        let filterObj = this.createSystemconfigObj("filter", filterBody);
        systemConfigs.push(filterObj)
    }

    private createSystemconfigObj(key: string, value: object) :SystemConfig{
        let systemConfig: SystemConfig = new SystemConfig();
        systemConfig.key = key;
        systemConfig.value = JSON.stringify(value);
        return systemConfig;
    }

    private createFilterAndSortBody(product:Product, genderArr: any[], brandArr: any[], categoryArr: any[]) :void{
        if (genderArr.indexOf(product.gender)<0){
            genderArr.push(product.gender);
        }
        if (brandArr.indexOf(product.brand)<0){
            brandArr.push(product.brand);
        }
        if (categoryArr.indexOf(product.category)<0){
            categoryArr.push(product.category);
        }
    }

    private parseProductAndCreateObj(productJson: any, products: Product[]): Product{
        let productObj: Product = new Product();
        
        productObj.additionalInfo = productJson["additionalInfo"];
        productObj.brand = productJson["brand"];
        productObj.catalogDate = productJson["catalogDate"];
        productObj.category = productJson["category"];
        productObj.colorVariantAvailable = productJson["colorVariantAvailable"];
        productObj.discount = productJson["discount"];
        productObj.discountDisplayLabel = productJson["discountDisplayLabel"];
        productObj.discountLabel = productJson["discountLabel"];
        productObj.discountType = productJson["discountType"];
        productObj.gender = productJson["gender"];
        productObj.landingPageUrl = productJson["landingPageUrl"];
        productObj.mrp = productJson["mrp"];
        productObj.price = productJson["price"];
        productObj.primaryColour = productJson["primaryColour"];
        productObj.productName = productJson["productName"];
        productObj.productUniqeId = productJson["productId"];
        productObj.rating = productJson["rating"];
        productObj.ratingCount = productJson["ratingCount"];
        productObj.searchImage = productJson["searchImage"];
        productObj.season = productJson["season"];
        productObj.sizes = productJson["sizes"];
        productObj.year = productJson["year"];
        products.push(productObj);
        return productObj;
    }
    

    private parseImageAndCreateObject(imageJsons: any, product: Product, imageList: Avatar[]): void{
        for (const image of imageJsons) {
            let imageObj: Avatar = new Avatar();
            imageObj.view = image["view"];
            imageObj.src = image["src"];
            imageObj.product = product;
            imageList.push(imageObj);
        }

    }

    private parseInventoryInfoAndCreateObject(inventoryJsons: any, product: Product, 
                                             inventoryList: InventoryInfo[]
    ): void {
        for (const inventory of inventoryJsons) {
            let inventoryObj: InventoryInfo = new InventoryInfo();
            inventoryObj.skuId = inventory["skuId"];
            inventoryObj.label = inventory["label"];
            inventoryObj.inventory = inventory["inventory"];
            inventoryObj.available = inventory["available"];
            inventoryObj.product = product;
            inventoryList.push(inventoryObj);
        }         
    }

    private parseSystemAttributesAndCreateObject(systemInfoJsons: any, product: Product, 
                                                sysInfoList: SystemAttribute[]
    ): void {
        for (const systemInfo of systemInfoJsons) {
            let systemAttObj: SystemAttribute = new SystemAttribute();
            systemAttObj.attribute = systemInfo["attribute"];
            systemAttObj.value = systemInfo["value"];
            systemAttObj.product = product;
            sysInfoList.push(systemAttObj);
        } 
    }

    
}