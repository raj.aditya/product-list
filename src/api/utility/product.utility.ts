
import { SystemConfig } from '../models/SystemConfig';
import { SystemConfigService } from '../services/systemconfig.service';
import { Service } from 'typedi';
import { ProductService } from "../services/product.service";
import { In } from 'typeorm';

@Service()
export class ProductUtility {
    constructor(
        private productService: ProductService,
        private systemConfigService: SystemConfigService
    ) {}

    /**
     * getProductList
    */
    public getProductList(productParams: any, fields = [], relations=[]): Promise<any>{
        let limit = 0;
        let offset = 0;
        let count = 0;
        let searchJson = {}
        if (productParams['limit'] && productParams['limit']>0){
            limit = productParams['limit'];
        }
        if (productParams['offset'] && productParams['offset']>0){
            offset = productParams['offset'];
        }
        if (productParams['count'] && productParams['count']>0){
            count = productParams['count'];
        }
        if (productParams['search'] && productParams['search'].length>0){
            searchJson["productName"] = productParams['search'];
            searchJson["brand"] = productParams['search'];
        }
        let sortJson = this.processSortJson(productParams['sort']);
        let filterBody = this.processFilter(productParams['filter']);
        return this.productService.list(limit, offset, fields, relations, filterBody, searchJson, sortJson, count)
    }

    private processSortJson(sortKey) {
        let sortJson: any = {'productId': 'ASC'}
        switch (sortKey) {
            case 'price_desc':
                sortJson = {'price': 'DESC'}
                break;
            case 'price_asc':
                sortJson = {'price': 'ASC'}
                break;
            case 'popularity':
                sortJson = {'rating': 'DESC'}
                break;
            case 'discount':
                sortJson = {'discount': 'DESC'}
                break;
            default:
                break
        }
        return sortJson
    }

    private processFilter(filterStr) :any{
        let filterBody = [];
        let filterJson: any;
        if(filterStr){
            filterJson = JSON.parse(filterStr);
        }else {
            return filterBody;
        }
        Object.keys(filterJson).forEach(key => {
            if(Array.isArray(filterJson[key])){
                filterBody.push({
                    name: key,
                    value: In(filterJson[key])
                });
            } else {
                filterBody.push({
                    name: key,
                    value: filterJson[key]
                });
            }
            
        });
        return filterBody
    }

    /**
     * getProductDetail
    */
    public getProductDetail(pId, relations =[]) {
        let filterBody = {
            productId: pId
        };
        return this.productService.findOne({
            where: filterBody,
            relations: relations
        })
    }

    /**
     * getSystemConfigData
    */
    public getSystemConfigData(systemKey): Promise<SystemConfig>{
        let filterBody = {
            key: systemKey
        };
        return this.systemConfigService.findOne({
            where: filterBody
        })
    }

    
}