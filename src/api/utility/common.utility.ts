import { Service } from 'typedi';

@Service()
export class CommonUtility {
    constructor() {}

    public sendResponseBody(status: boolean, message: string, resStatus: number, 
        response: any, data = undefined
    ): any{
        let resBody = {
            status: status,
            message: message
        }
        if(data){
            resBody["body"] = data
        }
        return response.status(resStatus).send(resBody);
    }

    
}