

import { SystemConfig } from '../models/SystemConfig';
import { EntityRepository, Repository } from 'typeorm';


@EntityRepository(SystemConfig)
export class SystemConfigRepository extends Repository<SystemConfig> {}
