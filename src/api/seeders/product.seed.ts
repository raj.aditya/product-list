import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { reject, resolve } from 'bluebird';
import {Container} from 'typedi';
import { DbSetupUtility } from '../utility/db-setup.utility';
import { Product } from '../models/Product';
import { Avatar } from '../models/Avatar';
import { SystemAttribute } from '../models/SystemAttributes';
import { InventoryInfo } from '../models/InventoryInfo';
import { SystemConfig } from '../models/SystemConfig';

export default class ProductData implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    
    const dbSetupUtility= Container.get<DbSetupUtility>(DbSetupUtility);

    return connection.transaction(async transactionalEntityManager => {
      try {
        let productJson = await dbSetupUtility.getProductReponse();
        let dbObjects = dbSetupUtility.setDbFromProductDump(productJson);
        await transactionalEntityManager.getRepository(Product).createQueryBuilder().insert().
            into(Product).values(dbObjects['products']).execute()
        console.log("Product Done")
        await transactionalEntityManager.getRepository(Avatar).createQueryBuilder().insert().
              into(Avatar).values(dbObjects['imageList']).execute()
        console.log("Image Done")
        await transactionalEntityManager.getRepository(InventoryInfo).createQueryBuilder().insert().
              into(InventoryInfo).values(dbObjects['inventoryList']).execute()
        console.log("Inventory Done")
        await transactionalEntityManager.getRepository(SystemAttribute).createQueryBuilder().insert().
              into(SystemAttribute).values(dbObjects['sysInfoList']).execute()
        console.log("Sysinfo Done")
        await transactionalEntityManager.getRepository(SystemConfig).createQueryBuilder().insert().
              into(SystemConfig).values(dbObjects['systemConfigs']).execute()
        console.log("Seeding Successfull")
        return resolve(true)
      } catch (error) {
        console.log("Seeding Error:", error)
        return reject(false)
      }
      
    })
  }
}
