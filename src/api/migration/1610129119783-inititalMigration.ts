import {MigrationInterface, QueryRunner} from "typeorm";

export class inititalMigration1610129119783 implements MigrationInterface {
    name = 'inititalMigration1610129119783'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "inventory_infos" ("created_date" character varying NOT NULL, "modified_date" character varying, "uuid" character varying NOT NULL DEFAULT 'b263f18f-5276-4ad7-b44f-9f24334ca875', "inventory_info_id" SERIAL NOT NULL, "sku_id" bigint NOT NULL, "label" character varying NOT NULL, "inventory" bigint NOT NULL, "available" boolean NOT NULL, "product_id" integer, CONSTRAINT "PK_a10dcd1d2da86bc6aca7399a9e1" PRIMARY KEY ("inventory_info_id"))`);
        await queryRunner.query(`CREATE TABLE "system_attributes" ("created_date" character varying NOT NULL, "modified_date" character varying, "uuid" character varying NOT NULL DEFAULT 'b263f18f-5276-4ad7-b44f-9f24334ca875', "system_attribute_id" SERIAL NOT NULL, "attribute" character varying NOT NULL, "value" character varying NOT NULL, "product_id" integer, CONSTRAINT "PK_77cd206da3fb13becdc2ad6dcd0" PRIMARY KEY ("system_attribute_id"))`);
        await queryRunner.query(`CREATE TABLE "products" ("created_date" character varying NOT NULL, "modified_date" character varying, "uuid" character varying NOT NULL DEFAULT 'b263f18f-5276-4ad7-b44f-9f24334ca875', "product_id" SERIAL NOT NULL, "landing_page_url" character varying NOT NULL, "product_uniqe_id" bigint NOT NULL, "product" character varying NOT NULL DEFAULT '', "product_name" character varying NOT NULL, "brand" character varying NOT NULL, "search_image" character varying NOT NULL, "sizes" character varying NOT NULL, "gender" character varying NOT NULL, "primary_colour" character varying NOT NULL, "discount_label" character varying NOT NULL, "discount_display_label" character varying NOT NULL, "additional_info" character varying NOT NULL, "category" character varying NOT NULL, "discount_type" character varying, "season" character varying NOT NULL, "catalog_date" bigint NOT NULL, "color_variant_available" boolean NOT NULL, "year" integer NOT NULL, "mrp" numeric NOT NULL, "price" numeric NOT NULL, "discount" numeric NOT NULL, "rating" numeric NOT NULL, "rating_count" bigint NOT NULL, CONSTRAINT "PK_a8940a4bf3b90bd7ac15c8f4dd9" PRIMARY KEY ("product_id"))`);
        await queryRunner.query(`CREATE TABLE "avatars" ("created_date" character varying NOT NULL, "modified_date" character varying, "uuid" character varying NOT NULL DEFAULT 'b263f18f-5276-4ad7-b44f-9f24334ca875', "image_id" SERIAL NOT NULL, "view" character varying NOT NULL, "src" character varying NOT NULL, "product_id" integer, CONSTRAINT "PK_7b741c9e100f57bf8ea77c24318" PRIMARY KEY ("image_id"))`);
        await queryRunner.query(`CREATE TABLE "system_configs" ("created_date" character varying NOT NULL, "modified_date" character varying, "uuid" character varying NOT NULL DEFAULT 'b263f18f-5276-4ad7-b44f-9f24334ca875', "system_config_id" SERIAL NOT NULL, "key" character varying NOT NULL, "value" text NOT NULL, CONSTRAINT "UQ_5aff9a6d272a5cedf54d7aaf617" UNIQUE ("key"), CONSTRAINT "PK_b5f9018384f23bd322aafc6533b" PRIMARY KEY ("system_config_id"))`);
        await queryRunner.query(`ALTER TABLE "inventory_infos" ADD CONSTRAINT "FK_edf8a8b0aae7324f7f597683c81" FOREIGN KEY ("product_id") REFERENCES "products"("product_id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "system_attributes" ADD CONSTRAINT "FK_489fe4865141f804ae0b63a20db" FOREIGN KEY ("product_id") REFERENCES "products"("product_id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "avatars" ADD CONSTRAINT "FK_d67a05f2cb141a31ce2ff4ddacb" FOREIGN KEY ("product_id") REFERENCES "products"("product_id") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "avatars" DROP CONSTRAINT "FK_d67a05f2cb141a31ce2ff4ddacb"`);
        await queryRunner.query(`ALTER TABLE "system_attributes" DROP CONSTRAINT "FK_489fe4865141f804ae0b63a20db"`);
        await queryRunner.query(`ALTER TABLE "inventory_infos" DROP CONSTRAINT "FK_edf8a8b0aae7324f7f597683c81"`);
        await queryRunner.query(`DROP TABLE "system_configs"`);
        await queryRunner.query(`DROP TABLE "avatars"`);
        await queryRunner.query(`DROP TABLE "products"`);
        await queryRunner.query(`DROP TABLE "system_attributes"`);
        await queryRunner.query(`DROP TABLE "inventory_infos"`);
    }

}
