import 'reflect-metadata';
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class LoginRequest {

  @MaxLength(10, {
    message: 'password is maximum 20 character',
  })
  @MinLength(5, {
    message: 'password is minimum 5 character',
  })
  @IsNotEmpty({
    message: 'password is required',
  })
  public password: string;

  @IsNotEmpty({
    message: 'Phone Num is required',
  })
  public username: string;

}
