
import { IsNotEmpty } from 'class-validator';
import {
  Column, 
  Entity, 
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne
} from 'typeorm';

import { BaseModel } from './BaseModel';
import { Product } from './Product';

@Entity('inventory_infos')
export class InventoryInfo extends BaseModel {

  @PrimaryGeneratedColumn({ name: 'inventory_info_id' })
  @IsNotEmpty()
  public inventoryInfoId: number;
  
  @Column({ name: 'sku_id', nullable: false, type: 'bigint'})
  public skuId: number;

  @Column({ name: 'label', nullable: false, type: 'character varying' })
  public label: string;

  @Column({ name: 'inventory', nullable: false, type: 'bigint' })
  public inventory: number;

  @Column({ name: 'available', nullable: false, type: 'boolean' })
  public available: boolean;

  @ManyToOne(type => Product, product=>product.inventoryInfos, {onDelete: 'SET NULL'})
  @JoinColumn({ name: 'product_id'})
  product: Product;

}
