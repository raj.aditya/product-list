
import { IsNotEmpty } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';

import { BaseModel } from './BaseModel';
import { Product } from './Product';

@Entity('avatars')
export class Avatar extends BaseModel {

  @PrimaryGeneratedColumn({ name: 'image_id' })
  @IsNotEmpty()
  public avatarId: number;

  @Column({ name: 'view', nullable: false, type: 'character varying' })
  public view: string;

  @Column({ name: 'src', nullable: false, type: 'character varying' })
  public src: string;

  
  @ManyToOne(type => Product, product=>product.images, {onDelete: 'SET NULL'})
  @JoinColumn({ name: 'product_id'})
  product: Product;
}
