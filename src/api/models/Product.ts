
import { IsNotEmpty } from 'class-validator';
import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';

import { BaseModel } from './BaseModel';
import { Avatar } from "./Avatar";
import { InventoryInfo } from './InventoryInfo';
import { SystemAttribute } from './SystemAttributes';

@Entity('products')
export class Product extends BaseModel {

  @PrimaryGeneratedColumn({ name: 'product_id' })
  @IsNotEmpty()
  public productId: number;

  @Column({ name: 'landing_page_url', nullable: false, type: 'character varying' })
  public landingPageUrl: string;

  @Column({ name: 'product_uniqe_id', nullable: false, type: 'bigint' })
  public productUniqeId: number;

  @Column({ name: 'product', nullable: false, type: 'character varying', default: '' })
  public product: string;

  @Column({ name: 'product_name', nullable: false, type: 'character varying' })
  public productName: string;

  @Column({ name: 'brand', nullable: false, type: 'character varying' })
  public brand: string;

  @Column({ name: 'search_image', nullable: false, type: 'character varying' })
  public searchImage: string;

  @Column({ name: 'sizes', nullable: false, type: 'character varying' })
  public sizes: string;

  @Column({ name: 'gender', nullable: false, type: 'character varying' })
  public gender: string;

  @Column({ name: 'primary_colour', nullable: false, type: 'character varying' })
  public primaryColour: string;

  @Column({ name: 'discount_label', nullable: false, type: 'character varying' })
  public discountLabel: string;

  @Column({ name: 'discount_display_label', nullable: false, type: 'character varying' })
  public discountDisplayLabel: string;

  @Column({ name: 'additional_info', nullable: false, type: 'character varying' })
  public additionalInfo: string;

  @Column({ name: 'category', nullable: false, type: 'character varying' })
  public category: string;

  @Column({ name: 'discount_type', nullable: true, type: 'character varying' })
  public discountType: string;

  @Column({ name: 'season', nullable: false, type: 'character varying' })
  public season: string;

  @Column({ name: 'catalog_date', nullable: false, type: 'bigint' })
  public catalogDate: string;

  @Column({ name: 'color_variant_available', nullable: false, type: 'boolean' })
  public colorVariantAvailable: boolean;

  @Column({ name: 'year', nullable: false })
  public year: number;

  @Column({ name: 'mrp', nullable: false, type: 'decimal' })
  public mrp: number;

  @Column({ name: 'price', nullable: false, type: 'decimal'  })
  public price: number;

  @Column({ name: 'discount', nullable: false, type: 'decimal'  })
  public discount: number;

  @Column({ name: 'rating', nullable: false, type: 'decimal'  })
  public rating: number;

  @Column({ name: 'rating_count', nullable: false, type: 'bigint'  })
  public ratingCount: number;

  @OneToMany((type) => Avatar, (avatar) => avatar.product)
  public images: Avatar[];

  @OneToMany((type) => InventoryInfo, (inventoryInfo) => inventoryInfo.product)
  public inventoryInfos: InventoryInfo[];

  @OneToMany((type) => SystemAttribute, (systemAttribute) => systemAttribute.product)
  public systemAttributes: SystemAttribute[];

}
