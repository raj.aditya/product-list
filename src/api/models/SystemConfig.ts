
import { IsNotEmpty } from 'class-validator';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { BaseModel } from './BaseModel';

@Entity('system_configs')
export class SystemConfig extends BaseModel {

  @PrimaryGeneratedColumn({ name: 'system_config_id' })
  @IsNotEmpty()
  public systemConfigId: number;

  @Column({ name: 'key', nullable: false, type: 'character varying' , unique: true})
  public key: string;

  @Column({ name: 'value', nullable: false, type: 'text' })
  public value: string;

}
