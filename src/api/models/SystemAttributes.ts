
import { IsNotEmpty } from 'class-validator';
import {
  Column, 
  Entity, 
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne
} from 'typeorm';
import { BaseModel } from './BaseModel';
import { Product } from './Product';

@Entity('system_attributes')
export class SystemAttribute extends BaseModel {

  @PrimaryGeneratedColumn({ name: 'system_attribute_id' })
  @IsNotEmpty()
  public systemAttributeId: number;

  @Column({ name: 'attribute', nullable: false, type: 'character varying' })
  public attribute: string;

  @Column({ name: 'value', nullable: false, type: 'character varying' })
  public value: string;

  @ManyToOne(type => Product, product=>product.systemAttributes, {onDelete: 'SET NULL'})
  @JoinColumn({ name: 'product_id'})
  product: Product;

}
