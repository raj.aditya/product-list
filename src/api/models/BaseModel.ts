import { Column, BeforeInsert, BeforeUpdate } from 'typeorm';
import { Exclude } from 'class-transformer';
import dayjs from 'dayjs'
import { v4 } from "uuid";

export abstract class BaseModel {
  @Column({ name: 'created_date' })
  public createdDate: string;

  @Exclude()
  @Column({ name: 'modified_date', nullable: true })
  public modifiedDate: string;

  @Exclude()
  @Column({name: "uuid", default: v4()})
  uuid: string;

  @BeforeInsert()
  public async createDetails(): Promise<void> {
    this.createdDate = dayjs().format('YYYY-MM-DD HH:mm:ss');
  }

  @BeforeUpdate()
  public async updateDetails(): Promise<void> {
    this.modifiedDate = dayjs().format('YYYY-MM-DD HH:mm:ss');
  }
}
