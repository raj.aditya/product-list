import { Service } from 'typedi';
import {GenericService} from './Generic.service';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { SystemConfig } from '../models/SystemConfig';

@Service()
export class SystemConfigService extends GenericService {

  constructor(@Logger(__filename)  log: LoggerInterface) {
    super(log, SystemConfig)
  }
}
