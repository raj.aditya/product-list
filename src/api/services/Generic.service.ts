

import { LoggerInterface } from 'lib/logger';
import { ILike, getRepository, EntityManager } from "typeorm";


export class GenericService {
    protected genericRepository: any;
    protected model;
    constructor(
        
        public log: LoggerInterface,
        model
    ) {

        this.genericRepository = getRepository(model);
        this.model = model;
    }

    // find object
    public findOne(findCondition: any): Promise<any> {
        this.log.info('Find one object');
        return this.genericRepository.findOne(findCondition);
    }

    // Object list
    public list(
        limit = 0,
        offset = 0,
        select: any = [],
        relation: any = [],
        whereConditions: any = [],
        searchJson: any ={},
        sortJson: any,
        count: number | boolean
    ): Promise<any> {
        const condition: any = {};

        if (select && select.length > 0) {
            condition.select = select;
        }

        if (relation && relation.length > 0) {
            condition.relations = relation;
        }

        condition.where = {};

        if (whereConditions && whereConditions.length > 0) {
            whereConditions.forEach((item: any) => {
                condition.where[item.name] = item.value;
            });
        }
        if (searchJson) {
            Object.keys(searchJson).forEach(key => {
                condition.where[key] = ILike('%' + searchJson[key] + '%');
            });
        }

        if (limit && limit > 0) {
            condition.take = limit;
            condition.skip = offset;
        }

        if(sortJson){
            condition.order = sortJson;
        }
        console.log(condition);

        if (count) {
            return this.genericRepository.count(condition);
        }

        return this.genericRepository.find(condition);
    }

    // create Object
    public async create(object, entityManager: EntityManager = undefined): Promise<any> {
        this.log.info('Create a new object => ', object.toString());
        if (entityManager) {
            const newObject = entityManager.getRepository(this.model).save(object);
            return newObject;
        }
        const newObject = await this.genericRepository.save(object);
        return newObject;
    }

    // update Object
    public update(
        id: any,
        object: any,
        key,
        entityManager: EntityManager = undefined
    ): Promise<any> {
        this.log.info('Update a object');

        object.key = id;
        if (entityManager) {
            return entityManager.getRepository(this.model).save(object);
        }
        return this.genericRepository.save(object);
    }

    // delete object
    public async delete(id: number, entityManager: EntityManager=undefined): Promise<any> {
        this.log.info('Delete a object');
        if (entityManager) {
            const deletedObject = entityManager.getRepository(this.model).delete(id);
            return deletedObject;
        }
        const deletedObject = await this.genericRepository.delete(id);
        return deletedObject;
    }

    /**
     * deleteAll
     */
    public bulkDelete(key, id, entityManager=undefined) {
        let deletedRepo;
        deletedRepo = entityManager
            ? entityManager.getRepository(this.model)
            : this.genericRepository;
        const deleteKey = `${key} =:id`;
        const value = {};
        value['id'] = id;
        return deletedRepo
            .createQueryBuilder()
            .delete()
            .from(this.model)
            .where(deleteKey, value)
            .execute();
    }

    /**
     * deleteAll
     */
    public bulkCreate(objects, entityManager=undefined) {
        let createRepo;
        createRepo = entityManager
            ? entityManager.getRepository(this.model)
            : this.genericRepository;

        return createRepo
            .createQueryBuilder()
            .insert()
            .into(this.model)
            .values(objects)
            .execute();
    }
}
