import { Service } from 'typedi';
import {GenericService} from './Generic.service';
import { Product } from '../models/Product';
import { Logger, LoggerInterface } from '../../decorators/Logger';

@Service()
export class ProductService extends GenericService {

  constructor(@Logger(__filename)  log: LoggerInterface) {
    super(log, Product)
  }
}
