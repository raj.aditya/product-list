
#!/bin/bash

# any future command that fails will exit the script
set -e
SUPERUSER_QUERY="sudo -su postgres psql postgres -tAc \"SELECT (EXISTS (SELECT 1 FROM pg_roles WHERE rolname='${USER}' AND rolsuper='t'))::int\""
SUPERUSER_EXIST=$(eval "$SUPERUSER_QUERY")
if [[ $SUPERUSER_EXIST -ne 1 ]]
then
	echo "${USER} not Present"
	sudo -su postgres createuser ${USER} -s
else
	echo "${USER} Present"
fi

USER_QUERY="psql postgres -tAc \"SELECT (EXISTS (SELECT 1 FROM pg_roles WHERE rolname='block6'))::int\""
USER_EXIST=$(eval "$USER_QUERY")
if [[ $USER_EXIST -ne 1 ]]
then
	echo "block6 not Present"
	psql postgres  -c "CREATE USER block6 WITH PASSWORD 'password';"
else
	echo "block6 Present"
fi

DB_QUERY="psql postgres -tAc \"SELECT (EXISTS (SELECT 1 FROM pg_database WHERE datname='clothdata'))::int\""
DB_EXIST=$(eval "$DB_QUERY")
if [[ $DB_EXIST -eq 1 ]]
then
	echo "DB clothdata already exist"
	dropdb clothdata
fi
createdb clothdata -O block6
