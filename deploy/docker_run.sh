
#!/bin/bash

# any future command that fails will exit the script
set -e

cd /home/ubuntu
aws s3 cp s3://dummy007/$1/docker-compose.yml  ./docker-compose.yml --region ap-south-1
mkdir -p .docker && echo $2>.docker/pass
cat ~/.docker/pass | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
docker-compose stop && docker-compose pull && docker-compose up -d
